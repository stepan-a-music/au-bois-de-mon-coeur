\version "2.14.2"

\header {
  title = "Au bois de mon coeur"
  instrument = "Guitare et chant"
  composer = "Georges Brassens"
  tagline = "stepan@ithaca.fr"
}

<<
    \chords{
      \set chordChanges = ##t
      a1:m d2 f2:7 e2:7 d2:7 c2 e2:7
      a1:m d2 f2:7 e2:7 d2:7
      c2 g4 f8 e8:7 a1:m
      d1:m d1:m g1:7 c1
      c1 a1:7 d2:m a2:m f2:7 e2:7
      f2:7 e2:7
      cis1:m fis2 a2:7
    }

  \relative c' {
    #(set-accidental-style 'default 'Voice)
    \autoBeamOff
    \key c \major
    \time 4/4
    \repeat volta 4 { 
      a2 c2 d2 dis2 e4 r8 a8 \times 2/3 {d4 a4 d4} c,4 r8 c8 \times 2/3 {b4 e,4 b'4}
      a2 c2 d2 dis2 e4 r8 a8 \times 2/3 {d4 a4 d4} c,4 r8 a8 b4 e4 a,1 \bar "||"
      a'4 a8. a16 a8 a8 a8 a8 a2~ a8 a8 b8 c8 b2~ b8 a8 g8 fis8 g1
      r4 a,4 b4 c4 cis8 g'8 a8 bes8 a8 g8 f8 e8 f4 r8 e8 a,4 e'4
    }
    \alternative {
      { dis2 e2
      }
      { dis2 e2
      }
    }
    \bar "||" \key cis \minor 
    cis2 e2 fis2 g2 gis4 r8 cis,8 \times 2/3 {fis4 cis4 fis4} e4 r8 cis8 \times 2/3 {dis4 gis,4 dis'4}
    cis2 e2 fis2 g2 gis4 r8 cis,8 \times 2/3 {fis4 cis4 fis4} e4 r8 cis8 dis4 gis4 cis,1 \bar "|."
  }
  
  \addlyrics {
      Au bois d'Cla -- mart y'a des pe -- tit's fleurs, 
      Y'a des pe -- tit's fleurs,
      Y'a des co -- pains au, au bois d'mon coeur
      Au au bois d'mon coeur.

      Au fond d'ma cour j'suis re -- nom -- mé __
      Au fond d'ma cour, __ j'suis re -- nom -- mé
      J'suis re -- nom -- mé
      Pour a -- voir le coeur mal fa -- mé
      Le coeur mal fa -- mé \repeat unfold 2 { \skip 1 }

      % Fin
      Au bois d'Cla -- mart y'a des pe -- tit's fleurs, 
      Y'a des pe -- tit's fleurs,
      Y'a des co -- pains au, au bois d'mon coeur
      Au au bois d'mon coeur.
    }
  
  \addlyrics {
     Au bois d'Vin -- cennes y'a des pe -- tit's fleurs,
     Y'a des pe -- tit's fleurs,
     Y'a des co -- pains au, au bois d'mon coeur,
     Au au bois d'mon coeur.

     Quand y'a plus d'vin dans mon ton -- neau
     Quand y'a plus d'vin dans mon ton -- neau
     Dans mon ton -- neau, __
     Ils n'ont pas peur de boir' mon eau,
     De boi -- re mon eau.
  }

    \addlyrics {
     Au bois d'Meu -- don y'a des pe -- tit's fleurs,
     Y'a des pe -- tit's fleurs,
     Y'a des co -- pains au, au bois d'mon coeur,
     Au au bois d'mon coeur.

     Ils m'ac -- compagn' -- nt à la mai -- rie
     Ils m'ac -- compagn' -- nt à la mai -- rie
     À la mai -- rie, __
     Cha -- que fois que je me ma -- rie,
     Que je me ma -- rie.
  }
  \addlyrics {
    Au bois d'Saint -- Cloud y'a des pe -- tit's fleurs,
    Y'a des pe -- tit's fleurs,
    Y'a des co -- pains au, au bois d'mon coeur,
    Au au bois d'mon coeur.
    
    Chaqu' fois qu'je meurs fi -- dé -- le -- ment,
    Chaqu' fois qu'je meurs fi -- dé -- le -- ment,
    Fi -- dé -- le -- ment,
    Ils sui -- vent mon en -- ter -- re -- ment,
    Mon en- ter- \repeat unfold 2 { \skip 1 } re  -- ment
  }
>>
